# Automate Building PDF from LaTex file using gitlab CI/CD

To automate building PDF from your tex file you need to configure and add a gitlab-ci.yml file like the [example here](gitlab-ci.yml) to your git branch and push it.

After a successful build you can download your PDF from an url like\ 
https://gitlab.com/yourusername/yourreponame/-/jobs/artifacts/yourbranchname/raw/yourtexfilename.pdf?job=build

## Files

[gitlab-ci.yml](gitlab-ci.yml)\
[build/Dockerfile](build/Dockerfile)

## Docker

If you want to build your own Docker image locally or want to know what packages are installed in the docker image on [hub.docker](https://hub.docker.com/repository/docker/kevinmoilar/debian-lualatex) see the [Dockerfile](build/Dockerfile) in the build directory.
